# IceandStone template for editoria

This template will produce a 17*24 draft looking document.

Run the following commands in the terminal

1. `npm install`
2. `npm run kickstart`
3. `npm run start`

